# @hfyngvason's CI/CD templates

## DEPRECATED: `Helm-2to3.gitlab-ci.yml`

The new location for this template is in the [GitLab codebase](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Helm-2to3.gitlab-ci.yml). 
